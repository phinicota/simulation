# simulation
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/phinicota/simulation)](https://goreportcard.com/report/gitlab.com/phinicota/simulation) [![coverage report](https://gitlab.com/phinicota/simulation/badges/master/coverage.svg)](https://gitlab.com/phinicota/simulation/commits/master) [![pipeline status](https://gitlab.com/phinicota/simulation/badges/master/pipeline.svg)](https://gitlab.com/phinicota/simulation/commits/master)

### Package docs

https://godoc.org/gitlab.com/phinicota/simulation
