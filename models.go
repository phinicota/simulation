package simulation

import (
	"fmt"
	"strings"
)

// Model represents a named struct that can have
// references to a set of other named Models (children).
type Model struct {
	name          string
	children      []*Model
	childrenTable map[string]*Model
}

// NewModel creates a NewModel named `name`.
func NewModel(name string) *Model {
	return &Model{
		name:          name,
		children:      make([]*Model, 0),
		childrenTable: make(map[string]*Model),
	}
}

// AddChild registers a new Model as a child of the current Model.
func (mod *Model) AddChild(child *Model) {
	mod.childrenTable[child.name] = child
	mod.children = append(mod.children, child)
}

// GetModel returns a reference a child by name, if it exists.
// If there is no child found, it returns false and a nil pointer.
func (mod *Model) GetModel(name string) (model *Model, ok bool) {
	// FIXME should return an error and nil if model doesn't exist!
	model, ok = mod.childrenTable[name]
	return
}

// GetChildren returns the children from a Model.
func (mod *Model) GetChildren() []*Model {
	return mod.children
}

// String converts Model to a representative string.
func (mod Model) String() string {
	return mod.name
}

func (mod Model) prefixTree(prefix string) (str string) {
	str = fmt.Sprintf("%v\n", mod)
	r := strings.NewReplacer("├", "│",
		"─", " ",
		"└", " ")
	rPrefix := r.Replace(prefix)

	children := mod.GetChildren()
	nChildren := len(children)
	for i, child := range children {
		treeChar := "├"
		if i >= nChildren-1 {
			// last child
			treeChar = "└"
		}
		childPrefix := rPrefix + treeChar + "─"
		str += fmt.Sprintf("%v%v",
			childPrefix,
			child.prefixTree(childPrefix),
		)
	}

	return
}

// Tree returns a string visualization of the model children structure.
func (mod Model) Tree() (str string) {
	return mod.prefixTree("")
}
