package simulation

import (
	"testing"
)

var (
	modelTree  = makeModelTree()
	realPtrMap = func() map[string]*Model {
		return make(map[string]*Model)
	}()
)

func makeModelTree() (mt map[string][]string) {
	mt = make(map[string][]string)
	mt["Father"] = []string{"Child1", "Child2", "Child3"}
	mt["Child1"] = []string{"Grandson1", "Grandson2"}
	mt["Grandson2"] = []string{"Greatgrandson1", "Greatgrandson2"}
	mt["Child2"] = []string{"Grandson3"}

	// no children models
	mt["Child3"] = []string{}
	mt["Grandson1"] = []string{}
	mt["Grandson3"] = []string{}
	mt["Greatgrandson1"] = []string{}
	mt["Greatgrandson2"] = []string{}

	return
}

func createHierarchy(modelName string) (model *Model) {
	model = NewModel(modelName)
	if childrenNames, ok := modelTree[model.name]; ok {
		for _, childName := range childrenNames {
			tempPtr := createHierarchy(childName)
			realPtrMap[childName] = tempPtr
			model.AddChild(tempPtr)
		}
	}
	return
}

func checkChildrens(t *testing.T, model *Model) {
	if childrenName, ok := modelTree[model.name]; ok {
		for _, childName := range childrenName {
			var (
				childPtr      *Model
				modelChildren = model.GetChildren()
			)

			nChildren := len(modelChildren)
			nExpectedChildren := len(childrenName)
			if nChildren != nExpectedChildren {
				t.Fatalf(`Number of children is different than expected in test case\n
						  Expected: %v. Found %v`, nChildren, nExpectedChildren)
			}

			// first check child model was added
			for _, child := range modelChildren {
				if child.name == childName {
					childPtr = child
				}
			}
			if childPtr == nil {
				t.Fatalf("Expected to find %v in %v children", childName, model.name)
			}

			// now check GetModel()
			storedChildPtr, ok := model.GetModel(childName)
			if childPtr != storedChildPtr {
				t.Fatalf("GetModel(%v) returns different pointer for stored model (named %v)?", childName, childName)
			}
			if !ok {
				t.Fatalf("GetModel(%v) failed to find model named %v", childName, childName)
			}
		}
	} else {
		t.Fatalf("%v model not found in test case.\n Maybe not added yet?", model.name)
	}
}

var treeStr = `root
└─Father
  ├─Child1
  │ ├─Grandson1
  │ └─Grandson2
  │   ├─Greatgrandson1
  │   └─Greatgrandson2
  ├─Child2
  │ └─Grandson3
  └─Child3
`

func TestModelTree(t *testing.T) {
	rootModel := NewModel("root")
	rootModel.AddChild(createHierarchy("Father"))
	if rootModel.Tree() != treeStr {
		t.Fatal("failed to example model tree")
	}

	for _, checkedModel := range realPtrMap {
		checkChildrens(t, checkedModel)
	}
}
