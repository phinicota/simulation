package simulation

import (
	"fmt"
	"sort"
	"sync"
	"time"
)

// An Event represents something that happens periodically.
type Event struct {
	name                 string
	period               time.Duration
	remainingRepetitions int
	nextTime             time.Time
	callback             func(wg *sync.WaitGroup)
}

// String satisfies Stringer interface
func (event *Event) String() string {
	return fmt.Sprintf("%v(period:%v, remain:%v, next:%v)",
		event.name, event.period, event.remainingRepetitions,
		event.nextTime)
}

// HappensBetween returns true if event happens between startingTime and endingTime.
func (event *Event) HappensBetween(
	startingTime, endingTime time.Time) bool {
	return (event.nextTime.Equal(startingTime) ||
		(event.nextTime.After(startingTime) &&
			event.nextTime.Before(endingTime))) &&
		(event.remainingRepetitions < 0 || event.remainingRepetitions > 0)
}

// SetCallback can be used to change the callback function that is called
// when an Event is ran by the Scheduler.
func (event *Event) SetCallback(function func()) {
	event.callback = func(wg *sync.WaitGroup) {
		defer wg.Done()
		event.nextTime = event.nextTime.Add(event.period)
		if event.remainingRepetitions > 0 {
			event.remainingRepetitions--
		}
		function()
	}
}

// NewEvent creates a new Event.
// Events are defined by:
//   * dt: period of time between Event ocurrences
//   * totalRepetitions: number of times Event occurs
//   * function: callback function executed when Event occurs
func NewEvent(name string,
	dt time.Duration,
	totalRepetitions int,
	function func()) (ev *Event) {
	var evPtr = &Event{
		name:                 name,
		period:               dt,
		remainingRepetitions: totalRepetitions,
	}
	evPtr.SetCallback(function)
	return evPtr
}

// Scheduler class determines which registred Events will
// run on each simulation step.
type Scheduler struct {
	events      []*Event
	step        time.Duration
	currentTime time.Time
}

// NewScheduler creates a new Scheduler
// Schedulers are defined by their simulation step duration,
// which determines the minimum time simulated.
func NewScheduler(step time.Duration) (sched *Scheduler) {
	return &Scheduler{
		events:      make([]*Event, 0),
		step:        step,
		currentTime: time.Time{},
	}
}

// Setup sets the starting time of registred events.
// Setup needs to be called before simulating the first step.
func (sched *Scheduler) Setup(start time.Time) {
	sched.currentTime = start
	for _, event := range sched.events {
		event.nextTime = start.Add(event.period)
	}
}

// AddEvent registers a new event to the Scheduler.
func (sched *Scheduler) AddEvent(event *Event) {
	sched.events = append(sched.events, event)
}

// RunStep simulates a Step.
func (sched *Scheduler) RunStep() {
	nextSchedulerTime := sched.currentTime.Add(sched.step)

	var (
		wg              sync.WaitGroup
		runningEvents   []*Event
		remainingEvents = make([]*Event, len(sched.events))
	)

	copy(remainingEvents, sched.events)
	for len(remainingEvents) > 0 {

		runningEvents = remainingEvents
		// FIXME goroutines don't necessarily start in go sentence
		// should scheduler guarantee order inside step?
		// run in chronological order
		sort.Slice(runningEvents, func(i, j int) bool {
			return runningEvents[i].nextTime.After(runningEvents[j].nextTime)
		})

		remainingEvents = make([]*Event, 0)
		for _, event := range runningEvents {
			if event.HappensBetween(
				sched.currentTime, nextSchedulerTime) {

				wg.Add(1)
				go event.callback(&wg)

				// could happen again in current step
				remainingEvents = append(remainingEvents,
					event)
			}
		}

		// wait for events to finish before
		// trying to run them again
		wg.Wait()
	}
	sched.currentTime = nextSchedulerTime
}
