package simulation

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func TestScheduler(t *testing.T) {
	sched := NewScheduler(100 * time.Millisecond)

	testEvents := map[*Event]int{}
	for i := 1; i <= 5; i++ {
		ev := NewEvent(fmt.Sprintf("event%v", i),
			time.Duration(rand.Float64()*400)*time.Millisecond,
			rand.Intn(6)+1,
			func() {},
		)

		ev.SetCallback(func() {
			// t.Logf(">> %v (remain:%v) | period:%v | nextTime:%v\n",
			// ev.name,
			// ev.remainingRepetitions,
			// ev.period,
			// ev.nextTime.Format("15:04:05.000"))
		})

		testEvents[ev] = ev.remainingRepetitions
		sched.AddEvent(ev)
	}

	startTime := time.Time{}
	sched.Setup(startTime)
	// for _, ev := range sched.events {
	// t.Logf("* created %v (remain:%v) | period:%v \n",
	// 	ev.name,
	// 	ev.remainingRepetitions,
	// 	ev.period,
	// )}

	// t.Log("Start Simulation:")
	for i := 0; i <= 50; i++ {
		// t.Logf("**** Time step: %v --> %v\n",
		// sched.currentTime.Format("15:04:05.000"),
		// sched.currentTime.Add(sched.step).Format("15:04:05.000"))
		sched.RunStep()
		for ev, totalRepet := range testEvents {
			expectedRepetitions := int(float64(i+1) * float64(sched.step) / float64(ev.period))
			if expectedRepetitions > 0 {
				if expectedRepetitions > totalRepet {
					// remove events from test once finished testing
					expectedRepetitions = totalRepet
					delete(testEvents, ev)
				}
				result := totalRepet - ev.remainingRepetitions
				if expectedRepetitions != result {
					t.Fatal("failed scheduler step test\n",
						ev.name, "should have run", expectedRepetitions, "/", totalRepet,
						"times but has ocurred", result, "times\n",
						"debug data:\n",
						"- event:", ev, "\n",
						"- time step: ", sched.currentTime.Format("15:04:05.000"),
						"-->", sched.currentTime.Add(sched.step).Format("15:04:05.000"), "\n",
						"- start time:", startTime.Format("15:04:05.000"))
				}
			}
		}
		if len(testEvents) == 0 {
			break
		}
	}

}
